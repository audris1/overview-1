# OSCAR - Open Source Supply Chains and Avoidance of Risk: An evidence based approach to improve FLOSS supply chains

The extracted data on dependencies in the format of 
```
repo;module1;...
```
is available at [this link](https://drive.google.com/drive/folders/1YxhXQMaQbGV9UY5iZFT-__-LzjsB7I3E?usp=sharing)

# Library dependencies for R

![R-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/R-modules.png)

# Module dependencies for Python

![PY-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/PY-modules.0.png)
![PY-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/PY-modules.1.png)
![PY-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/PY-modules.2.png)

# Package dependencies for Javascript

![JS-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/JS-modules.0.png)
![JS-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/JS-modules.1.png)
![JS-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/JS-modules.2.png)

# Package dependencies for C/C++

![C-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/C-modules.0.png)
![C-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/C-modules.1.png)
![C-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/C-modules.2.png)

# Package dependencies for C-sharp

![Cs-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/Cs-modules.0.png)
![Cs-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/Cs-modules.1.png)
![Cs-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/Cs-modules.2.png)

# Package dependencies for perl

![pl-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/pl-modules.0.png)
![pl-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/pl-modules.1.png)
![pl-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/pl-modules.2.png)

# Package dependencies for Go

![Go-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/Go-modules.0.png)
![Go-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/Go-modules.1.png)
![Go-modules.png](https://bitbucket.org/swsc/overview/raw/master/deps/Go-modules.2.png)




