# OSCAR - Open Source Supply Chains and Avoidance of Risk: An evidence based approach to improve FLOSS supply chains

The charts are for version O. First quarther of 2019 extrapolated for the entire year (by multiplying numbers by 4) 

![AuthorsByLanguageO.png](https://bitbucket.org/swsc/overview/raw/master/usage/AuthorsByLanguageO.png)
![PrdByLanguageO.png](https://bitbucket.org/swsc/overview/raw/master/usage/PrdByLanguageO.png)

Below is for version K:
![CommitsByLanguage.png](https://bitbucket.org/swsc/overview/raw/master/usage/commits_alphas.png)
![AuthorsByLanguage.png](https://bitbucket.org/swsc/overview/raw/master/usage/authors_alphas.png)
![PrdByLanguage.png](https://bitbucket.org/swsc/overview/raw/master/usage/avg_commits_alphas.png)

These trends are based on detecting file extensions associated with
commits modifying files. 

It seems that as language becomes more popular, the wider group of
contributors joins, lowering average productivity of a participant.


